# Import necessary libraries
import numpy as np
import matplotlib.pyplot as plt
import magpylib as magpy
from scipy.spatial.transform import Rotation as R
import os

# Parameter für Magnete und Anordnung
mag_width = 60
mag_length = 20
mag_height = 12
circle_diameter = 400  # Durchmesser in mm, entspricht 40 cm
circle_radius = circle_diameter / 2
num_magnets = 16
magnetization_strength = 1300
z_distance_between_magnets = 27  # Abstand zwischen den Magnetsets
xy_plane_z_position = 10  # Position der X-Y-Ebene entlang der Z-Achse, die dargestellt werden soll
counter = 1  # Initialisieren des Zählers für die Bildnamen

user_defined_style = {
    "show": True,
    "mode": "arrow+color",
    "size": 0.9,
    "arrow": {
        "color": "black",
        "offset": 0.8,
        "show": True,
        "size": 2,
        "sizemode": "scaled",
        "style": "solid",
        "width": 3,
    },
    "color": {
        "transition": 0,
        "mode": "tricolor",
        "middle": "white",
        "north": "magenta",
        "south": "turquoise",
    },
}


magpy.defaults.display.style.magnet.magnetization = user_defined_style


# Berechnung der Winkel und Positionen der Magnete im Kreis
angles = np.linspace(0, 360, num_magnets, endpoint=False)

magnets = []  # Liste, um alle Magnete zu speichern

# First set of 16 magnets with z_position = 0
for i, angle in enumerate(angles):
    rad_angle = np.radians(angle)
    x_position = circle_radius * np.cos(rad_angle)
    y_position = circle_radius * np.sin(rad_angle)
    position = (x_position, y_position, 0)  # z_position is 0 here

    magnetization_direction = ((-1) ** i) * magnetization_strength
    magnetization = (0, 0, magnetization_direction)

    magnet_orientation = R.from_euler('z', angle, degrees=True)

    magnet = magpy.magnet.Cuboid(
        magnetization=magnetization,
        dimension=(mag_width, mag_length, mag_height),
        position=position,
        orientation=magnet_orientation
    )

    magnets.append(magnet)

# Second set of 16 magnets with z_position = z_distance
for i, angle in enumerate(angles):
    rad_angle = np.radians(angle)
    x_position = circle_radius * np.cos(rad_angle)
    y_position = circle_radius * np.sin(rad_angle)
    position = (x_position, y_position, z_distance_between_magnets)  

    magnetization_direction = ((-1) ** (i + 1)) * magnetization_strength  # Change the direction for the second set
    magnetization = (0, 0, -magnetization_direction)

    magnet_orientation = R.from_euler('z', angle, degrees=True)

    magnet = magpy.magnet.Cuboid(
        magnetization=magnetization,
        dimension=(mag_width, mag_length, mag_height),
        position=position,
        orientation=magnet_orientation
    )

    magnets.append(magnet)
#magpy.show(*magnets, backend="plotly")

# Erstellen eines Meshgrids für die X-Y-Ebene mit gewünschtem Bereich
x = np.linspace(-400, 400, 120)
y = np.linspace(-400, 400, 120)

# Kombinieren aller Magnete zu einer Sammlung
collection = magpy.Collection(*magnets)

# Verzeichnis für gespeicherte Bilder erstellen, falls noch nicht vorhanden
if not os.path.exists('magnetic_field_images'):
    os.makedirs('magnetic_field_images')

# Schleife über Z-Werte von -60 bis 100
for z_value in range(-60, 101):
    print(f"Verarbeite Z-Wert: {z_value}")

    # Aktualisiere die Z-Position im Meshgrid
    X, Y = np.meshgrid(x, y)
    Z = np.ones_like(X) * z_value

    # Vorbereiten des Observers-Arrays mit der Form (..., 3)
    observers = np.stack((X, Y, Z), axis=-1)

    # Berechnen des Magnetfelds B auf dem Meshgrid
    B_field = collection.getB(observers)

    # Berechnen der Größe des B-Felds (Flussdichte)
    B_magnitude = np.linalg.norm(B_field, axis=2)

    # Berechne die Komponenten von B
    B_x = B_field[..., 0]
    B_y = B_field[..., 1]

    # Plotten der Feldlinien mit Streamplot
    plt.figure(figsize=(10, 10))
    linewidths = 0.5 + np.sqrt(B_magnitude) / B_magnitude.max() * 1.5
    stream = plt.streamplot(X, Y, B_x, B_y, color=B_magnitude, cmap='viridis', linewidth=linewidths, density=2, arrowstyle='-')
    plt.colorbar(stream.lines, label='Magnetische Flussdichte (mT)')
    plt.xlabel('X-Position (mm)')
    plt.ylabel('Y-Position (mm)')
    plt.title(f'B-Feld in der X-Y-Ebene bei Z = {z_value} mm')
    plt.axis('equal')
    plt.xlim(-300, 300)
    plt.ylim(-300, 300)

    # Bild speichern mit Zähler
    image_name = f'magnetic_field_images/{counter:01d}_B-Feld_Z_{z_value:03d}.png'
    plt.savefig(image_name)
    print(f"Bild gespeichert unter: {image_name}")

    # Schließe die Figur, um Speicher freizugeben
    plt.close()

    counter += 1  # Inkrementiere den Zähler nach jedem Durchgang

print("Alle Berechnungen abgeschlossen.")
